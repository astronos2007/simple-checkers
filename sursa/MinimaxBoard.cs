﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCheckers
{
    public class MinimaxBoard
    {
        public Board board { get; set; }
        public double eval { get; set; }
    }
}
