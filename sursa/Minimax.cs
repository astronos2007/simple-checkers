﻿/**************************************************************************
 *                                                                        *
 *  Copyright:   (c) 2016-2017, Florin Leon                               *
 *  E-mail:      florin.leon@tuiasi.ro                                    *
 *  Website:     http://florinleon.byethost24.com/lab_ia.htm              *
 *  Description: Game playing. Minimax algorithm                          *
 *               (Artificial Intelligence lab 8)                          *
 *                                                                        *
 *  This code and information is provided "as is" without warranty of     *
 *  any kind, either expressed or implied, including but not limited      *
 *  to the implied warranties of merchantability or fitness for a         *
 *  particular purpose. You are free to use this source code in your      *
 *  applications as long as the original copyright notice is included.    *
 *                                                                        *
 **************************************************************************/

using System;
using System.Collections.Generic;

namespace SimpleCheckers
{
    /// <summary>
    /// Implementeaza algoritmul de cautare a mutarii optime
    /// </summary>
    public class Minimax
    {
        private static Random _rand = new Random();
        public static int maxDepth = 2;

        /// <summary>
        /// Primeste o configuratie ca parametru, cauta mutarea optima si returneaza configuratia
        /// care rezulta prin aplicarea acestei mutari optime
        /// </summary>
        public static Board FindNextBoard(Board currentBoard)
        {
            // throw new Exception("Aceasta metoda trebuie implementata");
            // = algoritmul Minimax
            List<Board> listaConfiguratii = new List<Board>();
            List<Move> listaMutariValide;

            // parcurgem fiecare piesa de pe tabla
            foreach(Piece piesa in currentBoard.Pieces)
            {
                // doar in cazul pieselor computer-ului (nivelul maximizant)
                if (piesa.Player == PlayerType.Computer)
                {
                    // preluam lista de mutari valide ale piesei curente
                    listaMutariValide = piesa.ValidMoves(currentBoard);

                    // transformam aceasta lista de mutari intr-o lista de configuratii posibile
                    foreach(Move m in listaMutariValide)
                    {
                        listaConfiguratii.Add(currentBoard.MakeMove(m));
                    }
                }
            }

            // cautam mutarea pentru care functia de evaluare statica este maxima
            // consideram, initial, ca functia de evaluare este maxima in prima configuratie posibila
            double f_eval_max = listaConfiguratii[0].EvaluationFunction();
            double f_eval_curent;
            for (int i = 1; i < listaConfiguratii.Count; ++i)
            {
                f_eval_curent = listaConfiguratii[i].EvaluationFunction();
                if (f_eval_curent > f_eval_max)
                {
                    f_eval_max = f_eval_curent;
                }
            }

            // verificam daca exista mai multe maxime
            List<Board> listaConfiguratiiCuFEvalMaxim = new List<Board>();
            foreach(Board b in listaConfiguratii)
            {
                if (b.EvaluationFunction() == f_eval_max) // punem fiecare "cea mai buna configuratie" in lista
                {
                    listaConfiguratiiCuFEvalMaxim.Add(b);
                }
            }

            // daca exista mai multe configuratii cu aceeasi functie de evaluare maxima, returnam una aleatorie
            return listaConfiguratiiCuFEvalMaxim[_rand.Next(listaConfiguratiiCuFEvalMaxim.Count)];
        }

        /// <summary>
        /// Minimax -> Retezarea Alfa-Beta
        /// </summary>
        public static MinimaxBoard AlphaBetaPruning(MinimaxBoard node, int depth, double alfa, double beta, bool maximizingPlayer)
        {
            bool finished;
            PlayerType winner;
            node.board.CheckFinish(out finished, out winner);
            if (depth == 0 || finished) // am ajuns pe nod terminal, la limita de adancime impusa, sau jocul s-a incheiat
            {
                return node;
            }

            MinimaxBoard v = new MinimaxBoard();
            if (maximizingPlayer) // jucatorul maximizant = computer-ul
            {
                v.eval = Double.NegativeInfinity;

                // generam copiii nodului, adica configuratiile posibile valide
                List<Board> listaCopii = new List<Board>();
                List<Move> listaMutariValide;

                // parcurgem fiecare piesa de pe tabla
                foreach (Piece piesa in node.board.Pieces)
                {
                    // doar in cazul pieselor computer-ului (nivelul maximizant)
                    if (piesa.Player == PlayerType.Computer)
                    {
                        // preluam lista de mutari valide ale piesei curente
                        listaMutariValide = piesa.ValidMoves(node.board);

                        // transformam aceasta lista de mutari intr-o lista de configuratii posibile
                        foreach (Move m in listaMutariValide)
                        {
                            listaCopii.Add(node.board.MakeMove(m));
                        }
                    }
                }

                // parcurgem lista de copii
                foreach(Board child in listaCopii)
                {
                    MinimaxBoard childMB = new MinimaxBoard();
                    childMB.board = child;
                    childMB.eval = child.EvaluationFunction();

                    MinimaxBoard hBoard = AlphaBetaPruning(childMB, depth - 1, alfa, beta, false);
                    if (hBoard.eval > v.eval) // consideram valoarea maxima a functiei de evaluare
                    {
                        v = hBoard;
                        v.board = child; // aici se pastreaza copilul care duce catre cea mai buna frunza
                    }
                    if (v.eval > alfa) // alfa = max(alfa, v)
                    {
                        alfa = v.eval;
                    }

                    if (beta <= alfa)
                    {
                        break; // retezarea beta
                    }
                }
                
                return v;
            }
            else // jucatorul minimizant = omul
            {
                v.eval = Double.PositiveInfinity;

                // generam copiii nodului, adica configuratiile posibile valide
                List<Board> listaCopii = new List<Board>();
                List<Move> listaMutariValide;

                // parcurgem fiecare piesa de pe tabla
                foreach (Piece piesa in node.board.Pieces)
                {
                    // doar in cazul pieselor computer-ului (nivelul maximizant)
                    if (piesa.Player == PlayerType.Computer)
                    {
                        // preluam lista de mutari valide ale piesei curente
                        listaMutariValide = piesa.ValidMoves(node.board);

                        // transformam aceasta lista de mutari intr-o lista de configuratii posibile
                        foreach (Move m in listaMutariValide)
                        {
                            listaCopii.Add(node.board.MakeMove(m));
                        }
                    }
                }

                // parcurgem lista de copii
                foreach (Board child in listaCopii)
                {
                    MinimaxBoard childMB = new MinimaxBoard();
                    childMB.board = child;
                    childMB.eval = child.EvaluationFunction();

                    MinimaxBoard hBoard = AlphaBetaPruning(childMB, depth - 1, alfa, beta, true);
                    if (hBoard.eval < v.eval) // consideram valoarea maxima a functiei de evaluare
                    {
                        v = hBoard;
                        v.board = child; // aici se pastreaza copilul care duce catre cea mai buna frunza
                    }
                    if (v.eval < beta) // beta = min(beta, v)
                    {
                        beta = v.eval;
                    }

                    if (beta <= alfa)
                    {
                        break; // retezarea alfa
                    }
                }

                return v;
            }
        }

        /*
         * public static Board AlphaBetaPruning(Board node, int depth, double alfa, double beta, Board alfaBoard, Board betaBoard, bool maximizingPlayer)
        {
            bool finished;
            PlayerType winner;
            node.CheckFinish(out finished, out winner);
            if (depth == 0 || finished) // am ajuns pe nod terminal, la limita de adancime impusa, sau jocul s-a incheiat
            {
                return node;
            }

            double v;
            if (maximizingPlayer) // jucatorul maximizant = computer-ul
            {
                Board vBoard = new Board();
                vBoard.type = BoardType.NegativeInfinity;
                v = Double.NegativeInfinity;

                // generam copiii nodului, adica configuratiile posibile valide
                List<Board> listaCopii = new List<Board>();
                List<Move> listaMutariValide;

                // parcurgem fiecare piesa de pe tabla
                foreach (Piece piesa in node.Pieces)
                {
                    // doar in cazul pieselor computer-ului (nivelul maximizant)
                    if (piesa.Player == PlayerType.Computer)
                    {
                        // preluam lista de mutari valide ale piesei curente
                        listaMutariValide = piesa.ValidMoves(node);

                        // transformam aceasta lista de mutari intr-o lista de configuratii posibile
                        foreach (Move m in listaMutariValide)
                        {
                            listaCopii.Add(node.MakeMove(m));
                        }
                    }
                }

                // parcurgem lista de copii
                foreach(Board child in listaCopii)
                {
                    Board hBoard = AlphaBetaPruning(child, depth - 1, alfa, beta, alfaBoard, betaBoard, false);
                    if (hBoard.EvaluationFunction() > vBoard.EvaluationFunction()) // consideram valoarea maxima a functiei de evaluare
                    {
                        v = hBoard.EvaluationFunction();
                        vBoard = hBoard;
                    }
                    if (v > alfa) // alfa = max(alfa, v)
                    {
                        alfaBoard = vBoard;
                        alfa = v;
                    }

                    if (beta <= alfa)
                    {
                        break; // retezarea beta
                    }
                }
                
                return vBoard;
            }
            else // jucatorul minimizant = omul
            {
                Board vBoard = new Board();
                vBoard.type = BoardType.PositiveInfinity;
                v = Double.PositiveInfinity;

                // generam copiii nodului, adica configuratiile posibile valide
                List<Board> listaCopii = new List<Board>();
                List<Move> listaMutariValide;

                // parcurgem fiecare piesa de pe tabla
                foreach (Piece piesa in node.Pieces)
                {
                    // doar in cazul pieselor computer-ului (nivelul maximizant)
                    if (piesa.Player == PlayerType.Computer)
                    {
                        // preluam lista de mutari valide ale piesei curente
                        listaMutariValide = piesa.ValidMoves(node);

                        // transformam aceasta lista de mutari intr-o lista de configuratii posibile
                        foreach (Move m in listaMutariValide)
                        {
                            listaCopii.Add(node.MakeMove(m));
                        }
                    }
                }

                // parcurgem lista de copii
                foreach (Board child in listaCopii)
                {
                    Board hBoard = AlphaBetaPruning(child, depth - 1, alfa, beta, alfaBoard, betaBoard, true);
                    if (hBoard.EvaluationFunction() < vBoard.EvaluationFunction()) // consideram valoarea minima a functiei de evaluare
                    {
                        v = hBoard.EvaluationFunction();
                        vBoard = hBoard;
                    }
                    if (v < beta) // beta = min(beta, v)
                    {
                        betaBoard = vBoard;
                        beta = v;
                    }

                    if (beta <= alfa)
                    {
                        break; // retezarea alfa
                    }
                }
                
                return vBoard;
            }
        }
        */
    }
}
 