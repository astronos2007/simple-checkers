﻿/**************************************************************************
 *                                                                        *
 *  Copyright:   (c) 2016-2017, Florin Leon                               *
 *  E-mail:      florin.leon@tuiasi.ro                                    *
 *  Website:     http://florinleon.byethost24.com/lab_ia.htm              *
 *  Description: Game playing. Minimax algorithm                          *
 *               (Artificial Intelligence lab 8)                          *
 *                                                                        *
 *  This code and information is provided "as is" without warranty of     *
 *  any kind, either expressed or implied, including but not limited      *
 *  to the implied warranties of merchantability or fitness for a         *
 *  particular purpose. You are free to use this source code in your      *
 *  applications as long as the original copyright notice is included.    *
 *                                                                        *
 **************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleCheckers
{     
    /// <summary>
    /// Reprezinta o configuratie a jocului (o tabla de joc) la un moment dat
    /// </summary>
    public class Board
    {
        public int Size { get; set; } // dimensiunea tablei de joc
        public List<Piece> Pieces { get; set; } // lista de piese, atat ale omului cat si ale calculatorului
        public bool captureMade { get; set; } // daca s-a efectuat o captura in configuratia anterioara configuratiei curente
        
        public Board()
        {
            Size = 8;
            Pieces = new List<Piece>(Size * 3);

            int susJos = 0; // pentru dispunerea pe 2 randuri
            int id = 0;

            // piesele computer-ului
            for (int i = 0; i < Size; i++)
            {
                Pieces.Add(new Piece(i, Size - 1 - susJos, id++, PlayerType.Computer, PieceType.Checker));
                susJos = (susJos + 1) % 2;
            }
            for (int i = 0; i < Size; i += 2) // adaugam si randul 3, manual
            {
                Pieces.Add(new Piece(i, Size - 3, id++, PlayerType.Computer, PieceType.Checker));
            }

            // piesele omului
            susJos = 1;
            for (int i = 0; i < Size; i++)
            {
                Pieces.Add(new Piece(i, 0 + susJos, id++, PlayerType.Human, PieceType.Checker));
                susJos = (susJos + 1) % 2;
            }
            for (int i = 1; i < Size; i += 2) // adaugam si randul 3, manual
            {
                Pieces.Add(new Piece(i, 2, id++, PlayerType.Human, PieceType.Checker));
            }
        }

        public Board(Board b)
        {
            Size = b.Size;
            captureMade = b.captureMade;
            Pieces = new List<Piece>(Size * 2);

            foreach (Piece p in b.Pieces)
                Pieces.Add(new Piece(p.X, p.Y, p.Id, p.Player, p.Type));
        }

        /// <summary>
        /// Calculeaza functia de evaluare statica pentru configuratia (tabla) curenta
        /// </summary>
        public double EvaluationFunction()
        {
            // throw new Exception("Aceasta metoda trebuie implementata");

            double valoareEuristica = 0;
            
            // partea pentru jocul de inceput / de mijloc -> cand scopul principal este avansarea si incoronarea damelor
            foreach (Piece piesa in Pieces)
            {
                // functia de evaluare este considerata doar din punctul de vedere al calculatorului
                if (piesa.Player == PlayerType.Human)
                {
                    continue;
                }

                if (piesa.Type == PieceType.Checker)
                {
                    if (piesa.Y < Size / 2) // dama are valoarea 7 + nr_rand in jumatatea opusa computer-ului
                    {
                        valoareEuristica += 7;
                    }
                    else // si are valoarea 5 + nr_rand in jumatatea computer-ului
                    {
                        valoareEuristica += 5;
                    }
                }
                else if (piesa.Type == PieceType.King) // regele are valoarea 5 + nr_randuri + 2 indiferent de jumatatea in care se afla
                {
                    valoareEuristica += 10;
                }

                // pentru a avansa inspre centru
                if (piesa.X > 0 && piesa.X < Size - 1 && piesa.Y > Size - 3 && piesa.Y < Size - 1)
                {
                    valoareEuristica += 2;
                }

                // pentru a controla centrul tablei de joc
                if (piesa.X > 2 && piesa.X < Size - 2 && piesa.Y > 2 && piesa.Y < Size - 3) 
                {
                    valoareEuristica += 5;
                }

                // pentru a avansa inspre zona de incoronare (doar pentru dame)
                if (piesa.X > 0 && piesa.X < Size - 1 && piesa.Y > 0 && piesa.Y < 3 && piesa.Type == PieceType.Checker)
                {
                    valoareEuristica += 6;
                }

                // dam o importanta mare damelor cu posibilitate de incoronare, adica atunci cand sunt in penultimul rand, si pot muta pe ultimul
                if (piesa.Y == 0 && piesa.Type == PieceType.Checker)
                {
                    valoareEuristica += 15;
                }

                // daca vreo piesa de-a computer-ului este in pericol de a fi capturata, dezavantajam mult configuratia obtinuta prin aceasta mutare (-50)

                // pentru dreapta-sus
                if (piesa.X < Size - 1 && piesa.Y < Size - 1)
                {
                    foreach(Piece piesaOm in Pieces)
                    {
                        // daca exista vreun rege de-al omului in dreapta-sus, atunci piesa este in pericol
                        if (piesaOm.Player == PlayerType.Human && piesaOm.X == piesa.X + 1 && piesaOm.Y == piesa.Y + 1 && piesaOm.Type == PieceType.King)
                        {
                            valoareEuristica -= 50;
                            break;
                        }
                    }
                }
                // pentru stanga-sus
                if (piesa.X > 0 && piesa.Y < Size - 1)
                {
                    foreach (Piece piesaOm in Pieces)
                    {
                        // daca exista vreun rege de-al omului in stanga-sus, atunci piesa este in pericol
                        if (piesaOm.Player == PlayerType.Human && piesaOm.X == piesa.X - 1 && piesaOm.Y == piesa.Y + 1 && piesaOm.Type == PieceType.King)
                        {
                            valoareEuristica -= 50;
                            break;
                        }
                    }
                }
                // pentru dreapta-jos
                if (piesa.X < Size - 1 && piesa.Y > 0)
                {
                    foreach (Piece piesaOm in Pieces)
                    {
                        // daca exista vreo piesa de-a omului in dreapta-jos, atunci piesa este in pericol
                        if (piesaOm.Player == PlayerType.Human && piesaOm.X == piesa.X + 1 && piesaOm.Y == piesa.Y - 1)
                        {
                            valoareEuristica -= 50;
                            break;
                        }
                    }
                }
                // pentru stanga-jos
                if (piesa.X > 0 && piesa.Y > 0)
                {
                    foreach (Piece piesaOm in Pieces)
                    {
                        // daca exista vreo piesa de-a omului in stanga-jos, atunci piesa este in pericol
                        if (piesaOm.Player == PlayerType.Human && piesaOm.X == piesa.X - 1 && piesaOm.Y == piesa.Y - 1)
                        {
                            valoareEuristica -= 50;
                            break;
                        }
                    }
                }
            }

            // daca se poate efectua o captura, dam avantaj acestei mutari
            if (captureMade)
            {
                valoareEuristica += 2000;
            }

            // pentru partea de final, cand computer-ul are deja piese de tip regi
            // folosim comparatia intre numarul de regi ai computer-ului si numarul de regi ai omului
            int nrRegiComputer = 0;
            int nrRegiOm = 0;
            foreach(Piece piesa in Pieces)
            {
                if (piesa.Player == PlayerType.Computer) // pentru computer
                {
                    if (piesa.Type == PieceType.King) // numaram regii computer-ului
                    {
                        ++nrRegiComputer;
                    }
                }
                else // pentru om
                {
                    if (piesa.Type == PieceType.King) // numaram regii omului
                    {
                        ++nrRegiOm;
                    }
                }
            }

            if (nrRegiComputer == 0)
            {
                return valoareEuristica;
            }

            // daca se ajunge aici, computer-ul are regi si trebuie aplicata o strategie bazata pe distanta de la regi la piesele adversarului
            List<int> valoareEuristicaDistantePanaLaPieseleOmului = new List<int>();
            int valoareEuristicaDistanteCurenta;
            foreach(Piece piesaComputer in Pieces)
            {
                if (piesaComputer.Player == PlayerType.Computer && piesaComputer.Type == PieceType.King)
                {
                    valoareEuristicaDistanteCurenta = 0;
                    // pentru fiecare piesa a omului, valoareEuristicam distanta pana la ea
                    foreach(Piece piesaOm in Pieces)
                    {
                        if (piesaOm.Player == PlayerType.Human)
                        {
                            // folosim distanta Manhattan
                            valoareEuristicaDistanteCurenta += Math.Abs(piesaComputer.X - piesaOm.X) + Math.Abs(piesaComputer.Y - piesaOm.Y);
                        }
                    }
                    valoareEuristicaDistantePanaLaPieseleOmului.Add(valoareEuristicaDistanteCurenta);
                }
            }

            // daca computer-ul are mai multi regi decat omul, atunci aplica o strategie ofensiva (ataca) = minimizeaza valoareEuristica
            if (nrRegiComputer >= nrRegiOm)
            {
                valoareEuristica = -valoareEuristica - valoareEuristicaDistantePanaLaPieseleOmului.Min();
                
            }
            // daca computer-ul are mai putini regi decat omul, atunci aplica o strategie defensiva (se apara) = maximizeaza valoareEuristica
            else if (nrRegiComputer < nrRegiOm)
            {
                valoareEuristica += valoareEuristicaDistantePanaLaPieseleOmului.Max();
            }

            return valoareEuristica;
        }

        /// <summary>
        /// Creaza o noua configuratie aplicand mutarea primita ca parametru in configuratia curenta
        /// </summary>
        public Board MakeMove(Move move)
        {
            Board nextBoard = new Board(this); // copy

            // daca se muta pe diagonala, trebuie sa facem si capturarea de piesa
            int piesaCapturata_X = -1, piesaCapturata_Y = -1;
            if (Math.Abs(move.NewX - nextBoard.Pieces[move.PieceId].X) == 2 && Math.Abs(move.NewY - nextBoard.Pieces[move.PieceId].Y) == 2)
            {
                if (move.NewX - nextBoard.Pieces[move.PieceId].X > 0 && move.NewY - nextBoard.Pieces[move.PieceId].Y > 0) // dreapta-sus
                {
                    piesaCapturata_X = move.NewX - 1;
                    piesaCapturata_Y = move.NewY - 1;
                }
                else if (move.NewX - nextBoard.Pieces[move.PieceId].X < 0 && move.NewY - nextBoard.Pieces[move.PieceId].Y > 0) // stanga-sus
                {
                    piesaCapturata_X = move.NewX + 1;
                    piesaCapturata_Y = move.NewY - 1;
                }
                else if (move.NewX - nextBoard.Pieces[move.PieceId].X > 0 && move.NewY - nextBoard.Pieces[move.PieceId].Y < 0) // dreapta-jos
                {
                    piesaCapturata_X = move.NewX - 1;
                    piesaCapturata_Y = move.NewY + 1;
                }
                else if (move.NewX - nextBoard.Pieces[move.PieceId].X < 0 && move.NewY - nextBoard.Pieces[move.PieceId].Y < 0) // stanga-jos
                {
                    piesaCapturata_X = move.NewX + 1;
                    piesaCapturata_Y = move.NewY + 1;
                }

                // cautam piesa in lista de piese de pe tabla si o eliminam:
                for (int i = 0; i < nextBoard.Pieces.Count; ++i)
                {
                    Piece piesa = nextBoard.Pieces.ElementAt<Piece>(i);
                    if ((piesa.X == piesaCapturata_X) && (piesa.Y == piesaCapturata_Y))
                    {
                        piesa.X = -1;
                        piesa.Y = -1;
                        nextBoard.captureMade = true;
                        break;
                    }
                }
            }
            else
            {
                nextBoard.captureMade = false;
            }

            nextBoard.Pieces[move.PieceId].X = move.NewX;
            nextBoard.Pieces[move.PieceId].Y = move.NewY;

            // promovam piesa la rege, in caz ca a ajuns pe ultima linie
            if (nextBoard.Pieces[move.PieceId].Player == PlayerType.Human && move.NewY == Size - 1 && nextBoard.Pieces[move.PieceId].Type != PieceType.King)
            {
                nextBoard.Pieces[move.PieceId].Type = PieceType.King;
            }
            else if (nextBoard.Pieces[move.PieceId].Player == PlayerType.Computer && move.NewY == 0 && nextBoard.Pieces[move.PieceId].Type != PieceType.King)
            {
                nextBoard.Pieces[move.PieceId].Type = PieceType.King;
            }

            return nextBoard;
        }

        /// <summary>
        /// Verifica daca configuratia curenta este castigatoare
        /// </summary>
        /// <param name="finished">Este true daca cineva a castigat si false altfel</param>
        /// <param name="winner">Cine a castigat: omul sau calculatorul</param>
        public void CheckFinish(out bool finished, out PlayerType winner)
        {
            if (Pieces.Where(p => p.Player == PlayerType.Human && p.X == -1 && p.Y == -1).Count() == Size * 3 / 2)
            {
                finished = true;
                winner = PlayerType.Computer;
                return;
            }

            if (Pieces.Where(p => p.Player == PlayerType.Computer && p.X == -1 && p.Y == -1).Count() == Size * 3 / 2)
            {
                finished = true;
                winner = PlayerType.Human;
                return;
            }

            finished = false;
            winner = PlayerType.None;
        }
    }
}