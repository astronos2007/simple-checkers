﻿/**************************************************************************
 *                                                                        *
 *  Copyright:   (c) 2016-2017, Florin Leon                               *
 *  E-mail:      florin.leon@tuiasi.ro                                    *
 *  Website:     http://florinleon.byethost24.com/lab_ia.htm              *
 *  Description: Game playing. Minimax algorithm                          *
 *               (Artificial Intelligence lab 8)                          *
 *                                                                        *
 *  This code and information is provided "as is" without warranty of     *
 *  any kind, either expressed or implied, including but not limited      *
 *  to the implied warranties of merchantability or fitness for a         *
 *  particular purpose. You are free to use this source code in your      *
 *  applications as long as the original copyright notice is included.    *
 *                                                                        *
 **************************************************************************/

using System;
using System.Collections.Generic;

namespace SimpleCheckers
{
    public enum PlayerType { None, Computer, Human };
    public enum PieceType { Checker, King };

    /// <summary>
    /// Reprezinta o piesa de joc
    /// </summary>
    public class Piece
    {
        public int Id { get; set; } // identificatorul piesei
        public int X { get; set; } // pozitia X pe tabla de joc
        public int Y { get; set; } // pozitia Y pe tabla de joc
        public PlayerType Player { get; set; } // carui tip de jucator apartine piesa (om sau calculator)
        public PieceType Type { get; set; } // ce fel de piesa este (dama sau rege) 

        public Piece(int x, int y, int id, PlayerType player, PieceType type)
        {
            X = x;
            Y = y;
            Id = id;
            Player = player;
            Type = type;
        }

        /// <summary>
        /// Returneaza lista tuturor mutarilor permise pentru piesa curenta (this) 
        /// in configuratia (tabla de joc) primita ca parametru
        /// </summary>
        public List<Move> ValidMoves(Board currentBoard)
        {
            // throw new Exception("Aceasta metoda trebuie implementata");
            List<Move> validMovesList = new List<Move>();

            // consideram toate mutarile posibile din jurul pozitiei curente
            for (int x = X - 2; x <= X + 2; ++x)
            {
                for (int y = Y - 2; y <= Y + 2; ++y)
                {
                    Move mutare = new Move(Id, x, y);

                    // si verificam care mutari sunt valide
                    if (IsValidMove(currentBoard, mutare))
                    {
                        validMovesList.Add(mutare);
                    }
                }
            }

            return validMovesList;
        }

        /// <summary>
        /// Testeaza daca o mutare este valida intr-o anumita configuratie
        /// </summary>
        public bool IsValidMove(Board currentBoard, Move move)
        {
            // throw new Exception("Aceasta metoda trebuie implementata");

            if (currentBoard.Pieces[move.PieceId].X == -1) // nu se poate muta o piesa scoasa din joc
            {
                return false;
            }

            // verificam ca mutarea sa fie la 2 patrate distanta pe diagonale
            if (!(Math.Abs(move.NewX - X) == 2 && Math.Abs(move.NewY - Y) == 2))
            {
                if (Math.Abs(move.NewX - X) > 1 || Math.Abs(move.NewY - Y) > 1) // sau la doar 1 patrat distanta in orice directie
                {
                    return false;
                }
            }

            // verificam ca noile coordonate sa nu fie in afara tablei de joc
            if (move.NewX < 0 || move.NewX >= currentBoard.Size
                || move.NewY < 0 || move.NewY >= currentBoard.Size)
            {
                return false;
            }

            // verificam daca pe pozitia noua se afla vreo alta piesa
            foreach (Piece piesa in currentBoard.Pieces)
            {
                if (piesa.X == move.NewX && piesa.Y == move.NewY)
                {
                    return false;
                }
            }

            // verificam directia verticala (damele pot doar avansa, doar regii se pot si intoarce)
            if (currentBoard.Pieces[move.PieceId].Type == PieceType.Checker) // daca piesa este dama
            {
                if (currentBoard.Pieces[move.PieceId].Player == PlayerType.Human && move.NewY < currentBoard.Pieces[move.PieceId].Y)
                {
                    return false;
                }
                else if (currentBoard.Pieces[move.PieceId].Player == PlayerType.Computer && move.NewY > currentBoard.Pieces[move.PieceId].Y)
                {
                    return false;
                }
            }

            // in caz de mutare pe diagonala, peste 2 patrate, piesa peste care se sare trebuie sa fie a adversarului
            if (Math.Abs(move.NewX - X) == 2 && Math.Abs(move.NewY - Y) == 2)
            {
                bool existaPiesaPesteCareSeSare;
                int piesaPesteCareSeSare_X = -1, piesaPesteCareSeSare_Y = -1;

                if (move.NewX - X > 0 && move.NewY - Y > 0) // mutare spre dreapta-sus
                {
                    piesaPesteCareSeSare_X = X + 1;
                    piesaPesteCareSeSare_Y = Y + 1;
                }
                else if (move.NewX - X < 0 && move.NewY - Y > 0) // mutare spre stanga-sus
                {
                    piesaPesteCareSeSare_X = X - 1;
                    piesaPesteCareSeSare_Y = Y + 1;
                }
                else if (move.NewX - X > 0 && move.NewY - Y < 0) // mutare spre dreapta-jos
                {
                    piesaPesteCareSeSare_X = X + 1;
                    piesaPesteCareSeSare_Y = Y - 1;
                }
                else if (move.NewX - X < 0 && move.NewY - Y < 0) // mutare spre stanga-jos
                {
                    piesaPesteCareSeSare_X = X - 1;
                    piesaPesteCareSeSare_Y = Y - 1;
                }

                existaPiesaPesteCareSeSare = false;
                foreach (Piece piesa in currentBoard.Pieces) // verificam daca exista o piesa care va fi capturata
                {
                    if (piesa.X == piesaPesteCareSeSare_X && piesa.Y == piesaPesteCareSeSare_Y)
                    {
                        if (!isOppositePlayer(piesa.Player))
                        {
                            return false; // daca piesa peste care se sare nu este a adversarului, atunci mutarea este invalida
                        }
                        existaPiesaPesteCareSeSare = true;
                        break; // daca da, atunci mutarea este valida si nu mai cautam alte piese
                    }
                }
                if (!existaPiesaPesteCareSeSare)
                {
                    return false;
                }
            }

            // daca totul e in regula, mutarea este valida
            return true;
        }

        /// <summary>
        /// Verifica daca piesa data ca parametru apartine adversarului
        /// </summary>
        private bool isOppositePlayer(PlayerType p)
        {
            if (Player == PlayerType.Human && p == PlayerType.Computer)
            {
               return true;
            }
            if (Player == PlayerType.Computer && p == PlayerType.Human)
            {
                return true;
            }
            return false;
        }
    }
}